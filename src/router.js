import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Mapa Del Delito',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'Administración',
          path: 'pages/admin',
          component: () => import('@/views/dashboard/pages/Admin'),
        },
        {
          name: 'Informes',
          path: 'pages/notification',
          component: () => import('@/views/dashboard/pages/Notification'),
        },
        {
          name: 'DescargaDatos',
          path: 'pages/data',
          component: () => import('@/views/dashboard/pages/Data'),
        },
        {
          name: 'Tutorial',
          path: 'pages/tutorial',
          component: () => import('@/views/dashboard/pages/Tutorial'),
        },
        // Tables
        {
          name: 'Regular Tables',
          path: 'tables/regular-tables',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        // Maps
        {
          name: 'Rtl',
          path: 'pages/rtl',
          component: () => import('@/views/dashboard/pages/Rtl'),
        },
        // Upgrade
        {
          name: 'UserProfile',
          path: 'pages/userProfile',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
      ],
    },
  ],
})
